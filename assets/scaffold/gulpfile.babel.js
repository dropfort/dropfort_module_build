// This file is managed by dropfort/dropfort_module_build.
// Delete this file and run `composer update dropfort/dropfort_module_build` to regenerate it.

// Get gulp components and templates.
import { series, parallel, watch } from "gulp";
import { css, js, lib, sass } from "@coldfrontlabs/gulp-templates";

const { argv } = require("yargs/yargs")(process.argv.slice(2));

let mode = "";

try {
  mode = argv.mode;
} catch (error) {
  mode = "production";
}

const paths = {
  css: {
    src: "dist/css",
    dest: "dist/css",
    selector: "**/*.css",
  },
  js: {
    src: "js",
    dest: "dist/js",
    selector: "**/*.js",
  },
  lib: {
    src: [],
    dest: "dist/lib",
  },
  sass: {
    src: "scss",
    dest: "scss",
    selector: "**/*.scss",
  },
  min: "**/*.min.*",
  map: "**/*.map",
};

/**
 * Compiles all Sass files using dart sass and autoprefixer.
 *
 * @return {Object} - Gulp stream.
 */
export const compileStyles = () =>
  sass.compile({
    source: `${paths.sass.src}/${paths.sass.selector}`,
    destination: paths.css.dest,
    sourcemap: mode === "development",
  });
compileStyles.description =
  "Compiles all Sass files using dart sass and autoprefixer.";

/**
 * Compiles all JS files using Babel.
 *
 * @return {Object} - Gulp stream.
 */
export const compileScripts = () =>
  js.compile({
    source: `${paths.js.src}/${paths.js.selector}`,
    destination: paths.js.dest,
    sourcemap: mode === "development",
  });
compileScripts.description = "Compiles all JS files using Babel.";

/**
 * Minifies all CSS files.
 *
 * @return {Object} - Gulp stream.
 */
export const minifyStyles = () =>
  css.minify({
    source: [
      `${paths.css.src}/${paths.css.selector}`,
      `!${paths.min}`,
      `!${paths.map}`,
    ],
    destination: paths.css.dest,
    sourcemap: mode === "development",
    sourcemapOptions: { loadMaps: true },
  });
minifyStyles.description = "Minifies all CSS files.";

/**
 * Minifies all JS files.
 *
 * @return {Object} - Gulp stream.
 */
export const minifyScripts = () =>
  js.minify({
    source: [
      `${paths.js.dest}/${paths.js.selector}`,
      `!${paths.min}`,
      `!${paths.map}`,
    ],
    destination: paths.js.dest,
    sourcemap: mode === "development",
    sourcemapOptions: { loadMaps: true },
  });
minifyScripts.description = "Minifies all JS files.";

/**
 * Gathers all required libraries.
 *
 * @return {Object} - Gulp stream.
 */
export const fetchLibs = () =>
  lib.fetch({
    source: paths.lib.src,
    destination: paths.lib.dest,
    sourceOptions: { base: "./node_modules/" },
  });
fetchLibs.description = "Gathers all required libraries.";

/**
 * A placeholder function to run as an alternative to other tasks.
 *
 * The purpose of this function is to allow for the build task to
 * dynamically change the jobs it runs depending on envionment and sources.
 *
 * @param  {Function} callback - Gulps callback function.
 *
 * @return {Object} - Gulp stream.
 */
const placeholder = callback => callback();

/**
 * Compiles and minifies all Sass/CSS/JS files.
 *
 * @return {Object} - Gulp stream.
 */
export const build = parallel(
  series(compileStyles, minifyStyles),
  series(compileScripts, minifyScripts),
  paths.lib.src.length > 0 ? fetchLibs : placeholder
);
build.description = "Compiles and minifies all Sass/CSS/JS files.";

/**
 * Watches all Sass/JS files and compiles, and minifies them.
 */
function watchFiles() {
  watch(
    `${paths.sass.src}/${paths.sass.selector}`,
    series(compileStyles, minifyStyles)
  );
  watch(
    `${paths.js.src}/${paths.js.selector}`,
    series(compileScripts, minifyScripts)
  );
}
watchFiles.description =
  "Watches all Sass/JS files and compiles, and minifies them.";
export { watchFiles as watch };

// Create default tasks
export default build;
