module.exports = {
  "*.js": "eslint --fix",
  "*.{php,module,inc,install,test,profile,theme}":
    "phpcbf --standard=Drupal,DrupalPractice --extensions=php,module,inc,install,test,profile,theme",
};
