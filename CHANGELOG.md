# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 1.0.0 (2022-05-17)


### Features

* add initial scaffolding files ([27a1dc8](https://gitlab.com/dropfort/dropfort_module_build/commit/27a1dc8f666ae079aed91bb896e8c15f7caf93b6))
* **ci:** add feature npm security checks to scaffold ([98135a4](https://gitlab.com/dropfort/dropfort_module_build/commit/98135a4a586905b7c8ca63687e991b59ae05ebbc))
* **ci:** implement rule system for scaffolded ci files ([74060a9](https://gitlab.com/dropfort/dropfort_module_build/commit/74060a934a609c5035b75d485827e17d2cc761d8))
* rename to dropfort_module_build ([42d56a4](https://gitlab.com/dropfort/dropfort_module_build/commit/42d56a46b328cc5cc0fdb7eb77b49250e03ffebb))
* **scaffold:** add autoloader to gitignore ([749c7a8](https://gitlab.com/dropfort/dropfort_module_build/commit/749c7a8223d9accc1ca1c5481298a7afdc3b13a6))


### Bug Fixes

* **build:** update scaffolded postcss config ([9ed61a1](https://gitlab.com/dropfort/dropfort_module_build/commit/9ed61a1f8d8bf52db2c5cfb1032a32a83301d5ea))
* **ci:** correct ci scaffolding ([1296ece](https://gitlab.com/dropfort/dropfort_module_build/commit/1296ece8311ddea3387235a02ef295ce6bed3cb2))
* **ci:** correct composer outdated comparison ([6cfc53a](https://gitlab.com/dropfort/dropfort_module_build/commit/6cfc53ab646cf28117ad63e0c5fd34a6120a1d99))
* **ci:** correct reference to build jobs ([4087db1](https://gitlab.com/dropfort/dropfort_module_build/commit/4087db1380b103afabdd13f4f0400ea63d99d37b))
* **ci:** do not trigger pipelines with merge requests ([40b641c](https://gitlab.com/dropfort/dropfort_module_build/commit/40b641c7398a43830602673e5be1313ea5a52a66))
* **ci:** load deploy key in scaffold ci ([c66c06a](https://gitlab.com/dropfort/dropfort_module_build/commit/c66c06ab1b674e1d2ea04b2aea23da2908049052))
* **ci:** remove ssh key loading ([4ae9fbc](https://gitlab.com/dropfort/dropfort_module_build/commit/4ae9fbc8c984461fc33e9ec50c9a69db9e858054))
* **gulp:** remove lint from gulp scaffold ([e8ae3a0](https://gitlab.com/dropfort/dropfort_module_build/commit/e8ae3a0d4cd43193bbe76628ebadfd3de5e6e384))
* **npm:** correct dev build ([713d8c3](https://gitlab.com/dropfort/dropfort_module_build/commit/713d8c3b2b367d8911ad46b98908ae504e438fdd))
* **scaffold:** add missing files ([a38e580](https://gitlab.com/dropfort/dropfort_module_build/commit/a38e5807174d85db8665b7578ffb38f56f67d6de))
* **scaffold:** correct gitignore ([ec103ca](https://gitlab.com/dropfort/dropfort_module_build/commit/ec103ca0765e1ce1231c1742f386f4fbeb2bdc6a))
* **scaffold:** correct path to browserslistrc file ([6c588ec](https://gitlab.com/dropfort/dropfort_module_build/commit/6c588ec54df63621490116626b9b077f29b35ebb))
* **scaffold:** correct path to main ci ([6846e72](https://gitlab.com/dropfort/dropfort_module_build/commit/6846e72f61d859e5a4f7931d4bcca7c83291eb1c))
* **scripts:** remove parallel lint ([abe4ab9](https://gitlab.com/dropfort/dropfort_module_build/commit/abe4ab956b8b768a77f9dc69286387bdc02d7b7a))


### Performance Improvements

* **npm:** remove redundant prepare call ([2330a64](https://gitlab.com/dropfort/dropfort_module_build/commit/2330a64e19669b41e4f9c5d2e2c18683a7da43f7))


### Continuous Integration

* add project ci ([c108402](https://gitlab.com/dropfort/dropfort_module_build/commit/c1084023f0539ef8b9098e2cab40ad608859fb22))
* change template names to fit module ([5332f67](https://gitlab.com/dropfort/dropfort_module_build/commit/5332f6765e2f88865126ab4f04d6cc9aa7364375))
* fix auto update job ([fd19004](https://gitlab.com/dropfort/dropfort_module_build/commit/fd19004b51f5fa26c45064f835ebc0d122ab284e))
* fix jobs ([3f3127f](https://gitlab.com/dropfort/dropfort_module_build/commit/3f3127fefe522ce80b3f6f41080f98514fd9f3a1))
* fix paths ([3ba8813](https://gitlab.com/dropfort/dropfort_module_build/commit/3ba881331694ab6be3bc040ee5c189186359ca0a))


### Code Refactoring

* **npm:** update default build scripts ([5496b92](https://gitlab.com/dropfort/dropfort_module_build/commit/5496b92c05e060b2aade647b11bd3fe02d2d33cf))


### Build System

* add resolution for is-fullwidth-code-point 3.0.0 ([86998a4](https://gitlab.com/dropfort/dropfort_module_build/commit/86998a41cb0b9d2ea17903f2f7b797c90faf13da))
* **composer:** add core-composer-scaffold as a dependency ([e423250](https://gitlab.com/dropfort/dropfort_module_build/commit/e4232500493497c638f66148b41bb9bad7c1ce8b))
* **composer:** add php-compatability ([b75e378](https://gitlab.com/dropfort/dropfort_module_build/commit/b75e3786ee7ea0d36f058c0680f486330d6167b9))
* **composer:** add phpcs/phpcbf dependencies ([55fcba6](https://gitlab.com/dropfort/dropfort_module_build/commit/55fcba6ad7d981b45dbd2117725a64a68793f6a3))
* **npm:** update scaffolded package file ([876dcbd](https://gitlab.com/dropfort/dropfort_module_build/commit/876dcbd654d38b0b2b0d7c5d1f09e0534a359f32))
* **vscode:** update settings files ([67f1b0d](https://gitlab.com/dropfort/dropfort_module_build/commit/67f1b0d43d66ec2188c412bb1ae7876e41793286))
