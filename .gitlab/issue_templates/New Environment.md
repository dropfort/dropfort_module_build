## Required Information
- Type of environment? Dev, QA, Staging, Prod, etc:
- Domain name:
- Should we create a new server or use an existing one?

## To Do
If a new server is required:
- [ ] Send estimate for hosting.
- [ ] Received PO (or other confirmation paperwork) for hosting.
- [ ] Create the server
- [ ] Add server to inventory.yml

Configure site:
- [ ] Create/update Dropfort configuration.
- [ ] Register the domain name (and subdomain, if dev environment)
- [ ] Configure profile project in GitLab
- [ ] Configure CI pipeline and variables
- [ ] Deploy the site.
- [ ] Import database and copy files.
- [ ] Configure runner(s) for project.

## Resources
- https://git.dropfort.com/dropfort/documentation/blob/master/docs/server_config.md#create-the-server
- https://git.dropfort.com/dropfort/documentation/blob/master/docs/server_config.md#create-and-configure-the-server-on-dropfort
- https://git.dropfort.com/dropfort/documentation/blob/master/docs/domain_names.md
- https://git.dropfort.com/dropfort/drupal-templates/dropfort_drupal_template#initial-setup
- https://git.dropfort.com/dropfort/documentation/blob/master/docs/gitlab_config.md

## Notes
Domain name:
IP address:
Server name:
