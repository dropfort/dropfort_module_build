## Summary
*What is the bug you are experiencing?*

## Steps
*What are the steps to reproduce this bug?*

## Expected Behaviour
*What is the expected behaviour?*

## Environment
*What browser are you using?*
*What build/environment are you using? (please provide url and date)*

## To Do
- [ ] Fix bug
- [ ] Test (write automated test(s) if applicable)
- [ ] Briefly document the cause and the fix in comments.
- [ ] Reference issue in merge request